from django.shortcuts import render


def admin_chat(request):
    return render(request, "chat/admin.html")
