from django.urls import path

from .views import admin_chat

urlpatterns = [
    path('', admin_chat, name='admin-chat')
]
