from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.db import connection
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
import json,random,string

def query_toys_rent(s):
    c = connection.cursor()
    c.execute(s)
    result = c.fetchall()
    return result

def cud_toys_rent(s):
    c = connection.cursor()
    c.execute(s)
    rows_count = c.rowcount
    return rows_count

'''
DEFAULT STATUS
'''
def daftar_status():
    data = [
        'menunggu konfirmasi',
        'menunggu pembayaran',
        'sedang disiapkan',
        'sedang dikirim',
        'sedang disewa',
        'sudah dikembalikan',
        'batal'
    ]
    return data

'''
FOR PRODUCTION DELETE THIS CODE
THIS FUNCTION JUST FOR TESTING LIKE REAL USER
'''
def dummy_anggota():
    c = connection.cursor()
    c.execute("""
        SELECT P.no_ktp, P.nama_lengkap
        FROM PENGGUNA P
        JOIN ANGGOTA A ON A.no_ktp = P.no_ktp
        ORDER BY P.nama_lengkap;
    """)
    result = c.fetchall()
    c.close()
    return result

def generate_id_pemesanan():
    id_pemesanan = ''.join(random.choice(string.digits) 
    for _ in range(10))
    if id_pemesanan in dummy_anggota():
        id_pemesanan = generate_id_pemesanan()
    return id_pemesanan

def daftar_pesanan(request):
    url = "pesanan/pesanan_page.html"

    list_pesanan = query_toys_rent("""
        SELECT P.id_pemesanan, P.harga_sewa, P.status
        FROM PEMESANAN P
        JOIN BARANG_PESANAN BP ON P.id_pemesanan = BP.id_pemesanan
        ORDER BY P.id_pemesanan;
    """)
    list_barang = query_toys_rent("""
        SELECT P.id_pemesanan, B.nama_item
        FROM PEMESANAN P
        JOIN BARANG_PESANAN BP ON P.id_pemesanan = BP.id_pemesanan
        JOIN BARANG B ON B.id_barang = BP.id_barang
        ORDER BY P.id_pemesanan, B.nama_item;
    """)
    dict_pesanan = dict()
    for pesanan in list_pesanan:
        dict_pesanan[pesanan[0]] = {'id': pesanan[0], 'daftar_barang': [], 'harga': pesanan[1], 'status': pesanan[2]}
    for barang in list_barang:
        dict_pesanan[barang[0]]['daftar_barang'].append(barang[1])
    response = {'list_pesanan': dict_pesanan}
    return render(request, url ,response)



def buat_pesanan(request):
    url = "pesanan/form_pesanan_page.html"
    c = connection.cursor()
    anggota = dummy_anggota()
    c.execute("""
        SELECT * FROM (SELECT DISTINCT ON (B.id_barang) B.id_barang, B.nama_item,
            IB.harga_sewa FROM BARANG B
        JOIN INFO_BARANG_LEVEL IB ON IB.id_barang = B.id_barang
        ORDER BY B.id_barang, B.nama_item) AS S ORDER BY S.nama_item;
    """)
    barang = c.fetchall()
    c.close()
    return render(request, url, context={"barang": barang, "anggota": anggota})

def form_update(request, id_pemesanan):
    barang_pesanan_q = query_toys_rent("""
        SELECT BP.id_barang, BP.lama_sewa, BP.status
        FROM PEMESANAN P
        JOIN BARANG_PESANAN BP ON P.id_pemesanan = BP.id_pemesanan
        JOIN BARANG B ON B.id_barang = BP.id_barang
        WHERE P.id_pemesanan = '%s'
        ORDER BY P.id_pemesanan, B.nama_item;
    """ % (id_pemesanan))
    barang = query_toys_rent("""
        SELECT * FROM (SELECT DISTINCT ON (B.id_barang) B.id_barang, B.nama_item, IB.harga_sewa
        FROM BARANG B
        JOIN INFO_BARANG_LEVEL IB ON IB.id_barang = B.id_barang
        ORDER BY B.id_barang, B.nama_item) AS S ORDER BY S.nama_item;
    """)
    barang_pesanan = []
    for b in barang_pesanan_q:
        barang_pesanan.append(b[0])
    status_pesanan = barang_pesanan_q[0][2]
    lama_pesanan = barang_pesanan_q[0][1]
    status = query_toys_rent("""SELECT * FROM STATUS;""")
    response = {'id_pesanan': id_pemesanan,'barang_pesanan': barang_pesanan, 'status': status,
                'barang': barang, 'status_pesanan': status_pesanan, 'lama_pesanan': lama_pesanan}
    return render(request, 'pesanan/form_update_pesanan.html', response)

# def tambah_pesanan(request):
#     url = "pesanan/pesanan_page.html"
#     c = connection.cursor()
#     if request.method == "POST":
#         try:
#             id_pemesanan = generate_id_pemesanan()
#             id_anggota = request.POST.get("no_ktp")
#             barang_pesanan = request.POST.getlist(
#                 "barang[]")
#             lama_sewa = int(request.POST.get("lama_sewa"))
#             harga_sewa = float(request.POST.get('harga_sewa'))
#             kuantitas_barang = len(barang_pesanan)
#             timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#             ongkos = round(random.uniform(10000, 50000), 1)
#             status = daftar_status()[0]  # default menunggu konfirmasi
#             pesanan_inserted = cud_toys_rent("""
#                 INSERT INTO PEMESANAN VALUES
#                 ('%s', '%s', %d, %f, %f, '%s', '%s');
#             """ % (id_pemesanan, timestamp, kuantitas_barang, harga_sewa, ongkos, id_anggota, status))
#             tanggal_sewa = datetime.now().strftime("%Y-%m-%d")
#             str_barang_inserted = """
#                 INSERT INTO BARANG_PESANAN VALUES
#             """
#             for no_urut in range(kuantitas_barang):
#                 print(no_urut)
#                 if no_urut != 0:
#                     str_barang_inserted += ','
#                 str_barang_inserted += """
#                     ('%s', '%d', '%s', '%s', %d, %s, '%s')
#                 """ % (id_pemesanan, no_urut+1, barang_pesanan[no_urut], tanggal_sewa, lama_sewa, "null", status)
#             barang_inserted = cud_toys_rent(str_barang_inserted+';')
#             print(barang_inserted)
#             messages.success(request, "Berhasil menambah pesanan")
#         except:
#             messages.error(request, "Maaf telah terjadi kesalahan...")
#         finally:
#             c.close()
#         return HttpResponseRedirect(reverse('pesanan:daftar-pesanan'))
#     else:
#         return render(request, url)

def tambah_pesanan(request):
    id_anggota = request.POST.get('no_ktp')
    barang_pesanan = request.POST.getlist('list_barang[]')
    lama_sewa = int(request.POST.get('lama_sewa'))
    harga_sewa = float(request.POST.get('harga_sewa'))
    next_id = generate_id_pemesanan()
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M:%S")
    kuantitas = len(barang_pesanan)
    ongkos = round(random.uniform(10000, 50000), 1)
    status = daftar_status()[0]
    pesanan_inserted = cud_toys_rent("""
        INSERT INTO PEMESANAN VALUES
        ('%s', '%s', %d, %f, %f, '%s', '%s');
    """ % (next_id, timestamp, kuantitas, harga_sewa, ongkos, id_anggota, status))
    tgl_sewa = now.strftime("%Y-%m-%d")
    str_barang_inserted = """
        INSERT INTO BARANG_PESANAN VALUES
    """
    for no_urut in range(len(barang_pesanan)):
        if no_urut != 0:
            str_barang_inserted += ','
        str_barang_inserted += """
            ('%s', '%d', '%s', '%s', %d, %s, '%s')
        """ % (next_id, no_urut+1, barang_pesanan[no_urut], tgl_sewa, lama_sewa, "null", status)
    barang_inserted = cud_toys_rent(str_barang_inserted+';')
    return JsonResponse({'msg': 'oke', 'pesanan': pesanan_inserted, 'barang_pesanan': barang_inserted})


def update_pesanan(request):
    id_pesanan = request.POST.get('id_pesanan')
    barang_pesanan = request.POST.getlist('list_barang[]')
    lama_sewa = int(request.POST.get('lama_sewa'))
    harga_sewa = float(request.POST.get('total'))
    status = request.POST.get('status')
    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d %H:%M:%S")
    kuantitas = len(barang_pesanan)
    update_pesanan = cud_toys_rent("""
        UPDATE PEMESANAN SET datetime_pesanan='%s', kuantitas_barang=%d, harga_sewa=%f, status='%s'
        WHERE id_pemesanan='%s';
    """ % (timestamp, kuantitas, harga_sewa, status, id_pesanan))
    deleted_bp_row = cud_toys_rent("""
        DELETE FROM BARANG_PESANAN
        WHERE id_pemesanan = '%s';
    """ % (id_pesanan))
    tgl_sewa = now.strftime("%Y-%m-%d")
    str_update_barang = """
        INSERT INTO BARANG_PESANAN VALUES
    """
    for no_urut in range(len(barang_pesanan)):
        if no_urut != 0:
            str_update_barang += ','
        str_update_barang += """
            ('%s', '%d', '%s', '%s', %d, %s, '%s')
        """ % (id_pesanan, no_urut+1, barang_pesanan[no_urut], tgl_sewa, lama_sewa, "null", status)
    update_barang_pesanan = cud_toys_rent(str_update_barang+';')
    return JsonResponse({'msg': 'oke'})

@csrf_exempt
def hapus_pesanan(request):
    try:
        msg = 'ok'
        deleted_bp_row = 0
        deleted_p_row = 0
        id_pesanan = request.POST.get('id_pesanan')
        deleted_bp_row = cud_toys_rent("""
            DELETE FROM BARANG_PESANAN
            WHERE id_pemesanan = '%s' AND (status='menunggu konfirmasi' OR status='menunggu pembayaran');
        """ % (id_pesanan))
        deleted_p_row = cud_toys_rent("""
            DELETE FROM PEMESANAN
            WHERE id_pemesanan = '%s';
        """ % (id_pesanan))
        messages.success(request, "Berhasil menghapus pesanan")
    except:
        messages.error(request, "Maaf telah terjadi kesalahan...")

    return JsonResponse({'msg': msg, 'row_deleted': [deleted_bp_row, deleted_p_row]})
