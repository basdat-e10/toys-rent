# from django.db import models

# '''
# Note:
# - models.DO_NOTHING --> RAW SQL Integrity dan Constraint di handle
# - managed = False --> RAW SQL menyatakan table , django tidak terlibat

# '''

# class Status(models.Model):
#     nama = models.CharField(primary_key=True, max_length=50)
#     deskripsi = models.TextField()

#     class Meta:
#         managed = False
#         db_table = 'status'


# class Pemesanan(models.Model):
#     id_pemesanan = models.CharField(primary_key=True, max_length=10)
#     datetime_pesanan = models.DateTimeField(auto_now_add=True, null=False)
#     kuantitas_barang = models.IntegerField(null=False)
#     harga_sewa = models.FloatField()
#     ongkos = models.FloatField()
#     no_ktp_pemesan = models.ForeignKey(
#         Anggota, models.DO_NOTHING, db_column='anggota', null=False)
#     status = models.ForeignKey(Status, models.DO_NOTHING, db_column='status')

#     class Meta:
#         managed = False
#         db_table = 'pemesanan'


# class Barang_Pesanan(models.Model):
#     id_pemesanan = models.ForeignKey(
#         Pemesanan, models.DO_NOTHING, db_column='pemesanan', primary_key=True)
#     no_urut = models.CharField(max_length=10)
#     id_barang = models.ForeignKey(
#         Barang, models.DO_NOTHING, db_column='barang')

#     tanggal_sewa = models.DateField(null=False)
#     lama_sewa = models.IntegerField(null=False)
#     tanggal_kembali = models.DateField()
#     status = models.ForeignKey(
#         Status, models.DO_NOTHING, db_column='status', null=False)

#     class Meta:
#         managed = False
#         db_table = 'barang_pesanan'
#         unique_together = (('id_pemesanan', 'no_urut'),)
