from django.urls import path

from pesanan import views

app_name = "pesanan"

urlpatterns = [
    path('daftar-pesanan/', views.daftar_pesanan, name='daftar-pesanan'),
    path('buat-pesanan/', views.buat_pesanan, name='buat-pesanan'),
    path('form-update/<str:id_pemesanan>', views.form_update, name='form_update'),
    path('update-pesanan/', views.update_pesanan, name='update-pesanan'),
    path('hapus-pesanan/', views.hapus_pesanan, name='hapus-pesanan'),
    path('tambah-pesanan/', views.tambah_pesanan, name='tambah-pesanan'),
]
