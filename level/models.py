from django.db import models

'''
Note:
- models.DO_NOTHING --> RAW SQL Integrity dan Constraint di handle
- managed = False --> RAW SQL menyatakan table , django tidak terlibat
'''
class Level(models.Model):
    nama_level = models.CharField(primary_key=True, max_length=20)
    minimum_poin = models.FloatField(null=False)
    deskripsi = models.TextField()

    class Meta:
        managed = False
        db_table = 'level_keanggotaan'
        app_label = 'level'
