from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse, HttpResponseForbidden
from django.db import connection
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import json

from level.models import Level


def buat_level(request):
    url = "level/form_level_page.html"
    return render(request, url)


def api_level(request):
    c = connection.cursor()
    c.execute("SELECT * FROM level_keanggotaan;")
    result = c.fetchall()
    jsonObject = [dict(zip([key[0] for key in c.description], row))
                  for row in result]
    c.close()
    return HttpResponse(json.dumps({"data": jsonObject}), content_type="application/json")


def daftar_level(request):
    url = "level/level_page.html"
    return render(request, url)


def tambah_level(request):
    if(request.method == "POST"):
        c = connection.cursor()
        try:
            nama_level = request.POST.get('level')
            min_poin = float(request.POST.get('poin'))
            deskripsi = request.POST.get('deskripsi')
            c.execute("INSERT INTO level_keanggotaan(nama_level, minimum_poin, deskripsi) \
                 VALUES(%s, %s, %s)", [nama_level, min_poin, deskripsi])
            messages.success(request, "Berhasil menambah level")
        except:
            messages.error(request, "Maaf telah terjadi kesalahan...")
        finally:
            c.close()
        return HttpResponseRedirect(reverse('level:daftar-level'))
    else:
        return HttpResponseForbidden()

def update_level(request, nama_level):
    url = "level/update_level_page.html"
    c = connection.cursor()
    if(request.method == "POST"):
        try:
            old_nama_level = nama_level
            nama_level = request.POST.get('level')
            min_poin = float(request.POST.get('poin'))
            deskripsi = request.POST.get('deskripsi')
            c.execute("UPDATE level_keanggotaan set nama_level=%s, minimum_poin=%s,\
                 deskripsi=%s WHERE nama_level=%s", [nama_level, min_poin, deskripsi,old_nama_level])
            messages.success(request, "Berhasil update level")
        except:
            messages.error(request, "Maaf telah terjadi kesalahan...")
        finally:
            c.close()
        return HttpResponseRedirect(reverse('level:daftar-level'))

    c.execute("SELECT * FROM level_keanggotaan WHERE nama_level=%s",
        [nama_level])
    result = c.fetchall()
    jsonObject = [dict(zip([key[0] for key in c.description], row))
                  for row in result]
    c.close()
    return render(request, url, {"data": jsonObject})

@csrf_exempt
def hapus_level(request, nama_level):
    c = connection.cursor()
    try:
        c.execute(
            "DELETE FROM level_keanggotaan WHERE nama_level=%s", [nama_level])
        messages.success(request, "Berhasil menghapus level")
    except:
        messages.error(request, "Maaf telah terjadi kesalahan...")
    finally:
        c.close()
    return HttpResponseRedirect(reverse('level:daftar-level'))

    