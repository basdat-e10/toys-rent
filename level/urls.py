from django.urls import path

from level import views

app_name = "level"

urlpatterns = [
    path('api-level/', views.api_level, name='api-level'),
    path('buat-level/', views.buat_level, name='buat-level'),
    path('daftar-level/', views.daftar_level, name='daftar-level'),
    path('hapus-level/<str:nama_level>', views.hapus_level, name='hapus-level'),
    path('tambah-level/', views.tambah_level, name='tambah-level'),
    path('update-level/<str:nama_level>', views.update_level, name='update-level'),
]
