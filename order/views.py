from django.shortcuts import render


def item_list(request):
    return render(request, 'order/item-list.html')


def shopping_cart(request):
    return render(request, 'order/cart.html')
