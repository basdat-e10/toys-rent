from django.urls import path

from .views import item_list, shopping_cart

urlpatterns = [
    path('', item_list, name='item-list'),
    path('cart/', shopping_cart, name='shopping-cart')
]
