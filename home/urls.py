from django.urls import path

from .views import home_page, log_page, logout

urlpatterns = [
    path('', home_page, name='home'),
    path('login/', log_page, name='login'),
    path('register/', log_page, name='register'),
    path('logout/', logout, name='logout')
]
