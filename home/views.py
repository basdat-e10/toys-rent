import json
from django.contrib import messages
from django.contrib.auth import logout
from django.shortcuts import render, redirect
from .forms import UserAdminForm, UserCustomerForm


def home_page(request):
    return render(request, "home/homepage.html")


def log_page(request):
    if request.method == 'POST':
        data = request.POST
        if 'login' in data:
            messages.success(request, 'Login Successful!')
            return redirect('home')
        else:
            if 'admin' in data:
                form = UserAdminForm(data)
            else:
                form = UserCustomerForm(data)
            if form.is_valid():
                name = form.cleaned_data.get('name')
                messages.success(
                    request, f'Registration successful for {name}')
            return redirect('login')
    return render(request, "home/log_page.html", {
        'admin_form': UserAdminForm(),
        'user_form': UserCustomerForm()
    })


def logout(request):
    logout(request)
    return redirect('home')
