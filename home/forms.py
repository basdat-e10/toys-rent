from django.forms import ModelForm, DateField
from .models import UserAdmin, UserCustomer


class UserAdminForm(ModelForm):
    dob = DateField()

    def __init__(self, *args, **kwargs):
        super(UserAdminForm, self).__init__(*args, **kwargs)
        self.fields['ktp_num'].label = "KTP Number"
        self.fields['dob'].label = "Date of Birth"

    class Meta:
        model = UserAdmin
        fields = "__all__"


class UserCustomerForm(ModelForm):
    dob = DateField()

    def __init__(self, *args, **kwargs):
        super(UserCustomerForm, self).__init__(*args, **kwargs)
        self.fields['ktp_num'].label = "KTP Number"
        self.fields['dob'].label = "Date of Birth"

    class Meta:
        model = UserCustomer
        fields = "__all__"
        exclude = ('points',)
