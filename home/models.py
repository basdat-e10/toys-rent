from django.db import models


class UserAdmin(models.Model):
    ktp_num = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, unique=True)
    dob = models.DateField()
    phone_num = models.CharField(max_length=14)
    password = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class UserCustomer(models.Model):
    ktp_num = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255, unique=True)
    dob = models.DateField()
    phone_num = models.CharField(max_length=14)
    address = models.CharField(max_length=255)
    password = models.CharField(max_length=255)

    points = models.IntegerField()

    def __str__(self):
        return self.name
