if (location.pathname === '/register/') {
  document.getElementById('log-container').classList.add('right-panel-active')
}

const checkbox = document.getElementById('register-admin').getElementsByTagName('input')[0]
const userForm = document.getElementById('user-form')
const adminForm = document.getElementById('admin-form')
checkbox.addEventListener('change', e => {
  if (e.target.checked) {
    userForm.style.display = 'none'
    adminForm.style.display = 'flex'
  } else {
    userForm.style.display = 'flex'
    adminForm.style.display = 'none'
  }
})

document.getElementById('sign-up').addEventListener('click', () => {
  document.getElementById('log-container').classList.add('right-panel-active')
  window.history.pushState('', 'Register | Toys Rent', '/register/')
})

document.getElementById('sign-in').addEventListener('click', () => {
  document.getElementById('log-container').classList.remove('right-panel-active')
  window.history.pushState('', 'Login | Toys Rent', '/login/')
})
